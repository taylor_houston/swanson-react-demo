const baseConfig = {
    id: 'CHANGE_ME',
    viewports: [],
    scenarios: [],
    paths: {
        bitmaps_reference: 'CHANGE_ME',
        bitmaps_test: 'CHANGE_ME',
        engine_scripts: 'CHANGE_ME',
        html_report: 'CHANGE_ME',
        ci_report: 'CHANGE_ME'
    },
    fileNameTemplate: '{scenarioLabel}',
    asyncCaptureLimit: 30, //number of scenarios to run simultaneously
    resembleOutputOptions: { //options for diff image
        transparency: 0.25
    },
    casperFlags: [],
    engine: 'chrome',
    report: ['CI'],
    debugWindow: false,
    engineOptions: {
        typeInterval: 50 // wait 50ms between each type() keystroke
    }
}

const baseScenario = {
    label: 'CHANGE_ME',
    url: 'http://localhost:3000',
    hideSelectors: ['.nav-logo'],
    removeSelectors: [],
    selectorExpansion: true,
    selectors: ['document'], //'document' captures entire page. 'viewport' captures only visible viewport
    readyEvent: null,
    delay: 2000,
    misMatchThreshold: 0,
    requireSameDimensions: true
}

const config = (overrides) => {
    const config = Object.assign({}, baseConfig, overrides);
    config.paths = {
        bitmaps_reference: `backstop/${overrides.id}/images/reference`,
        bitmaps_test: `backstop/${overrides.id}/images/test`,
        engine_scripts: `backstop/scripts`,
        html_report: `backstop/${overrides.id}/reports/html`,
        ci_report: `backstop/${overrides.id}/reports/ci`
    };
    return config
}

const scenario = (overrides) => {
    const scenario = Object.assign({}, baseScenario, overrides)
    scenario.url = baseScenario.url + (overrides.url || '')
    return scenario
}

module.exports = {
    config,
    scenario,
}