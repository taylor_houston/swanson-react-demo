#!/usr/bin/env bash

#To run a subset of backstop config, provide an argument to our npm script
#Because our npm scripts already pass an argument in the package.json, the one we send will be the 2nd argument
#Ex: npm run backstop-test appointment
pkill -f "(chrome)?(--headless)"
backstop $1 --config=backstop/backstop.desktop.js --filter=$2
desktopExitCode=$?

pkill -f "(chrome)?(--headless)"
backstop $1 --config=backstop/backstop.mobile.js --filter=$2
smallExitCode=$?

#pkill -f "(chrome)?(--headless)"
#backstop $1 --config=backstop/backstop-small-landscape.config.js --filter=$2
#smallLandscapeExitCode=$?

#echo "Killing node now that backstop is finished..."
#pkill node

if  ["$smallExitCode" -ne "0"] || ["$desktopExitCode" -ne "0"]; then
    echo "\n************************************************************************"
    echo "Backstop test failures!"
    echo "************************************************************************\n"
    exit 1
fi
