const bs = require('./backstop.config.js')

const viewport = 'viewport'


module.exports = bs.config({
    id: 'small-view',
    viewports: [
        {
            name: 'small',
            width: 600,
            height: 3000
        }
    ],
    scenarios: [
        //Site Control pages (including old dashboard))
        bs.scenario({label: 'index-mobile', url: '/'}),
    ]
})
