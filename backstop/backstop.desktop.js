const bs = require('./backstop.config.js')

const viewport = 'viewport'


module.exports = bs.config({
    id: 'large-view',
    viewports: [{
        name: 'large',
        width: 1024,
        height: 3000
    }],
    scenarios: [
        //Site Control pages (including old dashboard))
        bs.scenario({label: 'index-desktop', url: '/'}),
    ]
})
