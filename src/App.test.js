import React from 'react';
import 'jsdom-global/register';
import {mount} from 'enzyme'
import {expect} from 'chai'
import App from './App';


it('renders without crashing', () => {
  const sut = mount(<App />)
  expect(sut).to.have.length(1)
});
