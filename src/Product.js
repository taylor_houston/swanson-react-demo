import React, { Component } from 'react';
import './Product.css';
class Product extends Component{

    constructor(props){
        super(props)

    }

    render(){
        return (
            <div className="product">
                <img src={`https://media.swansonvitamins.com/images/items/150/${this.props.productId}.jpg`} />
                <div>{this.props.productName}</div>
                <div>{this.props.price}</div>
            </div>
        )
    }

}

export default Product;