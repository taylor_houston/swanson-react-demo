import React from 'react';
import 'jsdom-global/register';
import {mount} from 'enzyme'
import {expect} from 'chai'
import Footer from './Footer';


it('renders without crashing', () => {
    const sut = mount(<Footer />)
    expect(sut).to.have.length(1)
});
