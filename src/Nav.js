import React, {Component} from 'react'
import logo from './logo.svg';
import Product from './Product';
class Nav extends Component{
    render(){
        return (
            <nav className="App">
                <header className="App-header">
                    <img src={logo} className="nav-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
            </nav>
        )
    }
}

export default Nav;