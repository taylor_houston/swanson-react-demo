import React from 'react';
import 'jsdom-global/register';
import {mount} from 'enzyme'
import {expect} from 'chai'
import Nav from './Nav';


it('renders without crashing', () => {
    const sut = mount(<Nav />)
    expect(sut).to.have.length(1)
});
