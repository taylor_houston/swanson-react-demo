import React from 'react';
import 'jsdom-global/register';
import {mount} from 'enzyme'
import {expect} from 'chai'
import ProductGrid from './ProductGrid';


it('renders without crashing', () => {
    const sut = mount(<ProductGrid />)
    expect(sut).to.have.length(1)
});
