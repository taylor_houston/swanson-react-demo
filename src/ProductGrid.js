import React, {Component} from 'react';
import './ProductGrid.css'
class ProductGrid extends Component{
    render(){
        return (
            <div className="product-grid">
                {this.props.children}
            </div>
        )
    }
}

export default ProductGrid;