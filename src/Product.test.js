import React from 'react';
import 'jsdom-global/register';
import {render} from 'enzyme'
import {expect} from 'chai'
import Product from './Product.js';


it('renders without crashing', () => {
    const sut = render(<Product />)
    expect(sut).to.have.length(1)
});
