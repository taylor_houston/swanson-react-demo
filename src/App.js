import React, { Component, Fragment } from 'react';
import './App.css';
import Nav from './Nav'
import Footer from './Footer'
import ProductGrid from './ProductGrid'
import Product from './Product'

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Nav />
            </header>
            <main>
                <ProductGrid>
                    <Product
                        price={9.99}
                        productName={"Tea or something"}
                        productId={"SWF123"}

                    />
                    <Product
                        price={12.99}
                        productName={"Black Pepper is da bomb"}
                        productId={"SW1613"}
                    />
                    <Product
                        price={13.99}
                        productName={"Milk Thistley"}
                        productId={"SW966"}
                    />
                    <Product
                        price={14.99}
                        productName={"CoQ10"}
                        productId={"SWU035"}

                    />
                    <Product
                        price={8.99}
                        productName={"Another Vitamin"}
                        productId={"SW106"}
                    />
                    <Product
                        price={11.99}
                        productName={"Real Food is good"}
                        productId={"SWU1036"}
                    />
                </ProductGrid>
            </main>
            <Footer />
        </Fragment>
    );
  }
}

export default App;
